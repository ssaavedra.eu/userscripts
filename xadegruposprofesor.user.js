// ==UserScript==
// @name         XADE Ver Grupos en Horarios Profesor
// @version      1.0.01
// @grant        none
// @namespace    http://ssaavedra.eu/
// @author       ssaavedra
// @homepageURL  https://gitlab.com/ssaavedra.eu/userscripts
// @downloadURL  https://gitlab.com/ssaavedra.eu/userscripts/-/raw/master/xadegruposprofesor.user.js
// @updateURL    https://gitlab.com/ssaavedra.eu/userscripts/-/raw/master/xadegruposprofesor.user.js
// @match        https://www.edu.xunta.es/xade/Inicio/mainSec.jsp
// @match        https://www.edu.xunta.es/xade/*
// @grant        none
// ==/UserScript==


function abreviarNomeGrupo(grupo) {
  try {
	  const re = /(\d).\s+(.).*-\s*([A-Z]+.*)/
  	const [_, n, g, b] = grupo.match(re)
    const i = g == 'P' ? "-" : "i"
  	return cloneInto(`${n}${i}${b}`, unsafeWindow)
  } catch(e) {
    return grupo
  }
}


// vyctorlm  29/01/2008
// Engado as instruccions para cargar o horario do primeiro cuadrimestre ou do segundo en funcion do que 
// estea elixido (por defecto 1º Cuadrimestre). Engado a informacion de PERIODOCONVOCATORIA as liñas das taboas.
function CubreListasHorariosFix()
{
  const getElementoPorNombre = unsafeWindow.principal.mainFrame.getElementoPorNombre
  const relacionHorarioProfesor = unsafeWindow.principal.mainFrame.relacionHorarioProfesor
  
    for (var i=1;i<=5;i++)
        unsafeWindow.principal.mainFrame.eval("lista"+i+".clearTabla()");

    var vectorAux;
    if (unsafeWindow.principal.mainFrame.getObjetoPorId('labelHorarioMostrado').innerHTML == 'Horario do 1º Cuadrimestre') {
    	vectorAux = unsafeWindow.principal.mainFrame.vectorHorarioProfesorCuadrimestre1;
    }
    else {
    	vectorAux = unsafeWindow.principal.mainFrame.vectorHorarioProfesorCuadrimestre2;
    }
    	
    var cont = 0;	
    if ((vectorAux != null)&&(vectorAux.length >0))
    {
        for (cont=0;cont <vectorAux.length;cont++)
        {
 
            var VectLinea = vectorAux[cont];			
            unsafeWindow.principal.mainFrame.lineas = cloneInto (new Array(), unsafeWindow);
            var lineas = unsafeWindow.principal.mainFrame.lineas
            var nomeGrupo = abreviarNomeGrupo(getElementoPorNombre(VectLinea,relacionHorarioProfesor,'NOMEGRUPO'))
            lineas[0] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'INICIO').substring(0,2)+':'+getElementoPorNombre(VectLinea,relacionHorarioProfesor,'INICIO').substring(2,4);
            lineas[1] = `${getElementoPorNombre(VectLinea,relacionHorarioProfesor,'NOMEMATERIA')} (${nomeGrupo})`;
            lineas['COD_HORARIO'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_HORARIO');
            lineas['COD_DIA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_DIA');
            lineas['COD_SESION'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_SESION');
            lineas['INICIO'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'INICIO');
            lineas['FIN'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'FIN');
            lineas['COD_TIPOMATERIA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_TIPOMATERIA');
            lineas['COD_MATERIA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_MATERIA');
            lineas['COD_SUBGRUPO'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_SUBGRUPO');
            lineas['COD_AULA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_AULA');
            lineas['NOMEMATERIA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'NOMEMATERIA');
            lineas['DESCRIPCIONMATERIA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'DESCRIPCIONMATERIA');
            lineas['NOMEGRUPO'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'NOMEGRUPO');
            lineas['DESCRIPCIONAULA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'DESCRIPCIONAULA');   

            // vyctorlm  19/03/2007
            // Engado a data de alta         
            lineas['DATAALTA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'DATAALTA');

            lineas['PERIODOCONVOCATORIA'] = getElementoPorNombre(VectLinea,relacionHorarioProfesor,'PERIODOCONVOCATORIA');

            unsafeWindow.principal.mainFrame.eval("lista"+getElementoPorNombre(VectLinea,relacionHorarioProfesor,'COD_DIA')+".addLineaSinRedibujar(unescapeArray(lineas))");
        }
    }
    for (var i=1;i<=5;i++)
    {
        unsafeWindow.principal.mainFrame.eval ( "var lista = lista"+i);
        unsafeWindow.principal.mainFrame.lista.displayTabla();
        if (unsafeWindow.principal.mainFrame.lista.lineas.length > 0) unsafeWindow.principal.mainFrame.lista.selectLinea(0);
      
      try {
        Array.prototype.slice.call(
          unsafeWindow.principal.mainFrame.document.getElementById('datos_'+unsafeWindow.principal.mainFrame.lista.id).querySelectorAll('div')
        ).map(e => {
          e.title = e.innerHTML;
        })
      } catch(e) { console.log("OUCH", e); }
    }
}


CubreListasHorariosFix.fixed = true

setInterval(function() {
    if(unsafeWindow.principal && unsafeWindow.principal.mainFrame) {
      if (unsafeWindow.principal.mainFrame.CubreListasHorarios && !unsafeWindow.principal.mainFrame.CubreListasHorarios.fixed) {
      	unsafeWindow.principal.mainFrame.CubreListasHorarios = exportFunction (CubreListasHorariosFix, unsafeWindow)
        unsafeWindow.principal.mainFrame.CubreListasHorarios.fixed = true
        unsafeWindow.principal.mainFrame.CubreListasHorarios()
      }
    } else {
    }
}, 5000)
