// ==UserScript==
// @name         Fix Wizink GA
// @namespace    http://ssaavedra.eu/
// @version      1.0.00
// @description  De-cripples Wizink when Google Analytics is not present
// @author       ssaavedra
// @homepageURL  https://gitlab.com/ssaavedra.eu/userscripts
// @downloadURL  https://gitlab.com/ssaavedra.eu/userscripts/-/raw/master/wizink-fix.user.js
// @updateURL    https://gitlab.com/ssaavedra.eu/userscripts/-/raw/master/wizink-fix.user.js
// @match        https://www.wizink.es/*
// @grant        none
// ==/UserScript==
// License: CC0

// Keep in mind: this script injects code into a banking website
// THIS SCRIPT COMES "AS IS" WITHOUT ANY WARRANTY, IMPLICIT OR OTHERWISE

function sendEvent () {}

unsafeWindow.sendEvent   = exportFunction (sendEvent, unsafeWindow);
