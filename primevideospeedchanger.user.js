// ==UserScript==
// @name         PrimeVideo/Netflix Speed Changer
// @namespace    http://ssaavedra.eu/
// @version      1.4.00
// @description  Allows changing the playback speed on Primevideo and Netflix
// @author       ssaavedra
// @homepageURL  https://gitlab.com/ssaavedra.eu/userscripts
// @downloadURL  https://gitlab.com/ssaavedra.eu/userscripts/-/raw/master/primevideospeedchanger.user.js
// @updateURL    https://gitlab.com/ssaavedra.eu/userscripts/-/raw/master/primevideospeedchanger.user.js
// @match        https://www.primevideo.com/*
// @match        https://www.netflix.com/*
// @grant        none
// ==/UserScript==
// License: CC0


(function() {
  'use strict';
  if(!window.location.href.match(/primevideo.com/)) {
    return;
  }

  let div = document.createElement('div')
  let input = document.createElement('input')
  let theVideo
  input.type = 'number'
  input.min = 0.5
  input.max = 4
  input.step = 0.25
  input.size = 2

  function changeFn() {
    if(theVideo) {
      console.log("New value", input.value)
      theVideo.playbackRate = input.value
    }
  }

  setInterval(function(){

    const videos = Array.prototype.slice.call(document.getElementsByTagName('video')).filter(v => v.src.match(/^blob:/))

    if(videos.length <= 0) {
      // console.log("No video in this page. Waiting until next tick.")
      return;
    }

    theVideo = videos[0]

    input.value = theVideo.playbackRate

    if(!div.parentNode) {
      div.appendChild(input)
      document.querySelector('.right .topButtons .hideableTopButtons').appendChild(div)
      input.addEventListener('change', changeFn, false)
    }


  }, 5000);
})();


(function() {
  'use strict';
  if(!window.location.href.match(/netflix.com/)) {
    return;
  }

  let div = document.createElement('div')
  let input = document.createElement('input')
  let theVideo
  input.type = 'number'
  input.min = 0.5
  input.max = 4
  input.step = 0.25
  input.size = 2
  input.style.fontSize = "22pt"
  input.style.color = "#000000"
  input.style.marginRight = "0.5em"

  function changeFn() {
    if(theVideo) {
      console.log("New value", input.value)
      theVideo.playbackRate = input.value
    }
  }

  function changeFnWheel (e) {
    console.log("Delta: ", e, e.deltaY)
    const val = parseFloat(input.value)
    if(e.deltaY > 0 && val < 4) {
      input.value = val + 0.25
    } else if (val > 0.5) {
      input.value = val - 0.25
    }
    theVideo.playbackRate = input.value
  }

  function attachedToBody(node) {
    if(node != null) {
      if(node == document.body) {
        return true
      } else {
        return attachedToBody(node.parentNode)
      }
    } else {
      return false
    }
  }

  let observer = new MutationObserver((mutations) => { 
    mutations.forEach((mutation) => {
      console.log("Mutation mutated:", mutation)
      const el = mutation.target;
      if ((!mutation.oldValue || !mutation.oldValue.match(/\bactive\b/)) 
          && mutation.target.classList 
          && mutation.target.classList.contains('active')){
        // Now we are active: attach div
        const adjacent = document.querySelectorAll('[data-uia=control-question]')[0].parentNode.parentNode
        adjacent.insertAdjacentElement('beforebegin', div)
        div.appendChild(input)
        input.addEventListener('change', changeFn, false)
        input.addEventListener('wheel', changeFnWheel, false)
      }
    });
  });


  setInterval(function(){

    const videos = Array.prototype.slice.call(document.getElementsByTagName('video')).filter(v => v.src.match(/^blob:/))

    if(videos.length <= 0) {
      return;
    }

    theVideo = videos[0]

    input.value = theVideo.playbackRate

    const div_attached = attachedToBody(div)
    if(!attachedToBody(div)) {
      window._inputDiv = div
      div.appendChild(input)
      input.addEventListener('change', changeFn, false)

      const element = document.querySelector('[data-uia=player]');
      observer.observe(element, { 
        attributes: true, 
        attributeOldValue: true, 
        attributeFilter: ['class'] 
      });
    }
  }, 5000);
})();
